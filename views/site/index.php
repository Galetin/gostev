<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\RegisterForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\widgets\MaskedInput;
use dosamigos\datetimepicker\DateTimePicker;
use unclead\multipleinput\MultipleInput;

$this->title = 'Запишитесь в наш фитнес клуб';

$model->verifyCode = '';

//debug($model->arrival);
?>
<div class="site-index">
                
    <?php if (Yii::$app->session->hasFlash('registerFormSubmitted')): ?>
           
    <div class="jumbotron">
        <div class="alert alert-success">
            Ваша заявка принята. Благодарим Вас за обращение. <br>Наш менеджер свяжется с вами в ближайщее время
        </div>
    </div>
    
    <?php else: ?>

    <div class="jumbotron">
        <h1><?= $this->title ?></h1>
        <span class="lead">Подготовте свое тело к пляжному сезону</span>
    </div>

    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            
            <?php if (Yii::$app->session->hasFlash('registerFormWarning')): ?>    
            <div class="alert alert-warning">
                <?= Yii::$app->session->getFlash('registerFormWarning') ?>
            </div>
            <?php endif; ?>
            
            <?php if (Yii::$app->session->hasFlash('registerFormError')): ?>
            <div class="alert alert-danger">
                <?= Yii::$app->session->getFlash('registerFormError') ?>
            </div>
            <?php endif; ?>

            <?php $form = ActiveForm::begin(['id' => 'register-form']); ?>

                <?= $form->field($model, 'name')/** /->textInput(['autofocus' => true])/**/ ?>

                <?= $form->field($model, 'phone')->widget(MaskedInput::className(), [ 'mask' => ['+7 (999) 999-99-99']]) ?>
                        
                <?= $form->field($model, 'email')/*->input('email')*/ ?>

                <?= $form->field($model, 'arrival')->widget(MultipleInput::className(), [
                        //'max'               => 7,
                        'min'               => 1,
                        'columns' => [
                            [
                                'name'  => 'timestamp',
                                'type'  => DateTimePicker::className(),
                                'options' => [
                                    'language' => 'ru',
                                    'template' => '{button}{input}{reset}',
                                    'pickButtonIcon' => 'glyphicon glyphicon-time',
                                    'clientOptions' => [
                                        'autoclose' => true,
                                        'startDate' => date('Y-m-d H:i', time() + (3600/2))
                                    ]
                                ]
                            ]
                        ],
                        'addButtonPosition' => MultipleInput::POS_FOOTER,
                    ])
                    //->label(false);
                ?>

                <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                    'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                ]) ?>

                <div class="form-group text-center">
                    <?= Html::submitButton('ЗАПИСАТЬСЯ', ['class' => 'btn btn-success', 'name' => 'register-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>

    <?php endif; ?>

</div>
