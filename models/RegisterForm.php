<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\Client;
use yii\validators\DateValidator;

class RegisterForm extends Model
{
    public $name;
    public $phone;
    public $email;
    public $arrival;
    public $verifyCode;
    
//    private $subject;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
           
            [['name', 'phone'], 'required' /**/, 'message' => 'Озязательное поле'/**/],
            
            [['name', 'phone', 'email'], 'filter', 'filter' => 'trim', 'skipOnArray' => true],
            
            ['name', 'string', 'length' => [3, 65]],
            
            ['name', 'filter', 'filter' => function ($value) {
                return preg_replace('/\s+/', ' ', $value);
            }],
            
            ['name', 'match', 'pattern' => '/^[а-яёa-zА-ЯЁA-ZІіЇїЄєҐґ\-\s]+$/u' ],

            ['phone', 'match', 'pattern' => '/^\+7\s\([0-9]{3}\)\s[0-9]{3}\-[0-9]{2}\-[0-9]{2}$/' ],

            ['email', 'email'],
                    
            ['arrival', 'validateTimestamp'],
 
            ['verifyCode', 'captcha'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'name' => 'ФИО',
            'phone' => 'Телефон',
            'arrival' => 'Дата и время посещения',
            'verifyCode' => 'Проверочный код:',
        ];
    }
    
    public function validateTimestamp($attribute)
    {
        $requiredValidator = new DateValidator();
        $requiredValidator->format = 'yyyy-MM-dd HH:mm';

        $timestamps = array_unique($this->$attribute['timestamp']);
        $validTimestamps = [];
        
        natsort($timestamps);

        foreach($timestamps as $index => $value) {
            if ( empty($value) ) continue;
            $error = null;
            
            $requiredValidator->validate($value, $error);
            
            if ( !empty($error) ) {
                $key = 'timestamp_' . $index;
                $this->addError($key, $error);
                Yii::$app->session->setFlash('registerFormError', 'Ошибка. Некорректный ввод «Дата и время»');
            }
            else $validTimestamps[] = $value;
        }
        
        $this->$attribute = $validTimestamps;
    }

    /**
     * @return bool whether the model passes validation
     */
    public function register()
    {
        if ($this->validate()) {
            
            $phone = '8' . preg_replace('/^\+7|\D/', '', $this->phone);
            
            $client = new Client();
            
            if( $client->findOne(['mobile' => $phone]) ){
                Yii::$app->session->setFlash('registerFormWarning', 'Такой номер телефона уже зарегистрирован!');
                return false;
            }
            
            $client->name = $this->name;
            $client->mobile = $phone;
            $client->email = $this->email;
            $client->arrival = implode("|", $this->arrival);
            
            if( $client->insert() ){
                
                Yii::$app->mailer->compose()
                    ->setTo(Yii::$app->params['adminEmail'])
                    ->setFrom(['noreply@energom-fitnes.test' => 'Robot'])
                    ->setSubject('Новый клиент ['.$this->name.']')
                    ->setHtmlBody('ФИО: '.$this->name.'<br>Тел.: '.$this->phone.'<br>E-mail: '.$this->email)
                    ->send();
                
                return true; 
            }
            else {
                Yii::$app->session->setFlash('registerFormError', 'Ошибка сервера. Попробуйте позже.');
            } 
        }
        return false;
    }
}
