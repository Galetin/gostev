<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * Description of Client
 *
 * @author Makarov Artem
 */
class Client extends ActiveRecord {
    
    public static function tableName() {
        return 'clients';
    }
}
